import pandas
import numpy as np

'''
Preprocesses the scraped Wikipedia texts in .npy files for each industry and builds
a large corpus that defines the top-level industries into a .npy file.
'''
# list of english stop words
stop_words = ["www", "t/ha", "\xa0kg", "e.g", "i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself", "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself", "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that", "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having", "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", "while", "of", "at", "by", "for", "with", "about", "against", "between", "into", "through", "during", "before", "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under", "again", "further", "then", "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", "each", "few", "more", "most", "other", "some", "such", "no", "nor", "not", "only", "own", "same", "so", "than", "too", "very", "s", "t", "can", "will", "just", "don", "should", "now"]

def stripper(corpus):
    # split corpus
    corpus = np.char.split(corpus, sep=' ')

    # split strings in corpus
    split_corpus = []
    n = len(corpus)
    for i in range(n):
        split_corpus.append(corpus[i])

    # flatten list
    corpus = []
    for sublist in split_corpus:
        for item in sublist:
            corpus.append(item)

    # remove weblinks
    corpus = [ word for word in corpus if word.startswith('https://') == False and word.startswith('www.') == False ]

    # process text by removeing punctuation
    n = len(corpus)
    for i in range(n):
        corpus[i] = corpus[i].lower()
        corpus[i] = corpus[i].replace('..', ' ')
        corpus[i] = corpus[i].replace('|', ' ')
        corpus[i] = corpus[i].replace('=', ' ')
        corpus[i] = corpus[i].replace('&', ' ')
        corpus[i] = corpus[i].replace(':', ' ')
        corpus[i] = corpus[i].replace('(', ' ')
        corpus[i] = corpus[i].replace(')', ' ')
        corpus[i] = corpus[i].replace('.', ' ')
        corpus[i] = corpus[i].replace('"', ' ')
        corpus[i] = corpus[i].replace('%', ' ')
        corpus[i] = corpus[i].replace("'s", '.')
        corpus[i] = corpus[i].strip("§\/><″+=“”’'`′~{£‘„—−|°·% ^€»_×,;²¿:]-–[)(±}{&*$#@!.0123456789")

    # remove anomalous occurances ("thumb|" and "category:")
    corpus = [ word for word in corpus if word.startswith('thumb|') == False and word.startswith('category:') == False]

    # remove stop words
    no_stop_words = []
    for word in corpus:
        if word not in stop_words:
            no_stop_words.append(word)

    #remove strings of len 2 and lower
    corpus = [ word for word in no_stop_words if len(word) >= 3 ]

    return corpus

def helper(input):
    corpus = stripper(input)
    corpus = stripper(corpus)

    # Merge all text into one string
    n = len(corpus)
    sentence = corpus[0]
    for i in range(1, n):
        sentence = sentence + " " + corpus[i]
    return sentence

corpus_list = []
for i in range(1, 33):
    # read doc as "corpus"
    corpus = np.load('data_files/scraped/huge/i' + str(i) + '.npy')

    # create a string containing all text about a top-level
    # industry and its sub industries and append it to a list
    sentence = helper(corpus)
    corpus_list.append(sentence)

np.save('data_files/total_massive_industries_corpus.npy', corpus_list)
