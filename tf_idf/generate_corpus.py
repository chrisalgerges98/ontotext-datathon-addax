import mwparserfromhell
import pywikibot
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
import numpy as np
import pandas as pd
import re
import ast

'''
For every top-level industry, this file scrapes the Wikipedia text of the
top-level industry and its sub industries and save it into a .npy file

INPUT: list of industries with all subindustries in each line
OUTPUT: for every industry a .npy file with all wikipedia text for all sub industries
'''

# Scrape the wikipedia text using pywikibot
# and extract clean textual data using MWParserFromHell
def parse(title):
    text = []
    while True:
        # Go to the wikipedia website, find the wikipedia page of 'title'
        # and extract the wikipedia text. Stop if there is no page or
        # the page does not contain any text
        try:
            site = pywikibot.Site()
            page = pywikibot.Page(site, title)
            text = page.get()
            False
            break
        except pywikibot.IsRedirectPage:
            break
        except pywikibot.NoPage:
            break

    # Remove noise from the scraped
    # wikipedia text
    return mwparserfromhell.parse(text)

with open("data_files/industry_subsectors_data.txt", "r") as kw:
    count = 0
    for line in kw:
        # Converts the list of companies, which is saved as
        # a string, into a list
        sectorlist = ast.literal_eval(line)
        sector_corpus = []

        # Scrape the wikipedia text of every sub industry and append
        # the text into a list
        for subsector in sectorlist:
            wikicode = parse(subsector)
            stripped = wikicode.strip_code()
            lines = stripped.splitlines()
            sector_corpus.extend(lines)
        count += 1

        # save the Wikipedia text of a industry as a .npy file
        filename = 'data_files/scraped/huge/i' + str(count) + '.npy'
        np.save(filename, sector_corpus)
