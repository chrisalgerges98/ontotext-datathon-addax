import numpy as np
import pandas as pd

'''
Creates 32 lists containing the top-level industries and its
sub industry and saves the lists into a text file.
'''

industries_df = pd.read_csv('data_files/industrysubsectors.csv')

topLevelIndustries = industries_df['toplevelindustry'].unique()

# creates for every industry a txt file and writes
# a list into the txt file containing the top-level industry and it's
# industry subsectors
for industry in topLevelIndustries:
    industry_df = industries_df[industries_df['toplevelindustry'] == industry]
    industry_subsectors = industry_df['industrysubsector'].to_list()
    industry_subsectors.insert(0, industry)

    # to reset the text file 'industry_subsectors_data.txt', remove it
    # from the folder 'data_files'.
    f = open('data_files/industry_subsectors_data.txt', 'a')
    f.write(str(list(set(industry_subsectors))) + '\n')
f.close()
