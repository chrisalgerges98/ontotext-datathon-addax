from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import pandas as pd

'''
Calculates the TF-IDF matrix, that will be used to compute the
word embeddings of the company keywords. It also extracts and saves
the vocabulary of the industries corpus
'''

corpus = np.load('data_files/total_massive_industries_corpus.npy')

# perform TF-IDF on the industries corpus
vectorizer = TfidfVectorizer(stop_words='english')
tfidf_matrix = vectorizer.fit_transform(corpus)
tfidf_dataframe = pd.DataFrame(tfidf_matrix.toarray(),
columns=vectorizer.get_feature_names())

# save the TF-IDF matrix and the vocabulary of the industries corpus
tfidf_dataframe.to_csv('data_files/TF-IDF_matrix.csv', index=False)
np.save('data_files/TF-IDF_words.npy', vectorizer.get_feature_names())
