import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from sklearn.manifold import TSNE

'''
Creates the t-SNE plot of the company vectors calculated using the
TF-IDF methodology
'''

companies_data_array = np.load('data_files/tsne_data.npy')

X = companies_data_array[:, 4] # company_vectors
y = companies_data_array[:, 3] # industries

# determine the locations of company vectors
# that contain NaN values
def determine_nan_vector_indices(company_vectors):
    nan_vector_indices = []
    for i, vector in enumerate(company_vectors):
        for value in vector:
            if np.isnan(value):
                nan_vector_indices.append(i)
    return sorted(list(set(nan_vector_indices)))

# Reshape the array into an easier shape to handle
# namely (amount_of_company_vectors, 32), where
# 32 represents the amount of top-level industries
X_new = []
for company_vector in X:
    company_vector = company_vector.reshape(1, 32)
    X_new.append(company_vector)
X2 = np.array(X_new).reshape(X.shape[0], 32)

# remove datapoints of comapny vectors containing NaN values
nan_vectors_indices = determine_nan_vector_indices(X2)
X_cleaned = np.delete(X2, nan_vectors_indices, 0)
y_cleaned = np.delete(y, nan_vectors_indices, 0)

# calculate t-SNE embeddings in 2 dimensions
X_embedded = TSNE(n_components=2).fit_transform(X_cleaned)

color_list = ['maroon', 'brown', 'olive', 'teal', 'navy', 'black', 'red', 'orange',
'yellow', 'lime', 'green', 'cyan', 'blue', 'purple', 'magenta', 'grey']
marker_list = ['+', 's']

labels = np.unique(y_cleaned)
color_index = 0
marker_index = 0

# plot the t-SNE embeddings of the company vectors,
# one industry cluster at a time
for i in range(len(labels)):
    indices = np.where(y_cleaned == labels[i])[0]
    plot_data = X_embedded[indices]
    plt.plot(plot_data[:, 0], plot_data[:, 1],
    marker_list[marker_index],
    color=color_list[color_index],
    label=labels[i])

    if color_index == (len(color_list) - 1):
        color_index = 0
        marker_index += 1
    else:
        color_index += 1

# Shows the t-SNE plot.
# Save the t-SNE plot manually into the folder tf_idf_plots
plt.legend()
plt.title('t-SNE plot TF-IDF company vectors')
plt.xlabel('t-SNE component 1')
plt.ylabel('t-SNE component 2')
plt.show()
