from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MultiLabelBinarizer
import numpy as np
import pandas as pd

'''
Identifies for each industry the false positives and the false negatives
by classifying the TF-IDF company vectors into the top-level industries.
The names of the companies, which are the false positives and the false negatives,
are stored into CSV files, one for every industry
'''

companies_data_array = np.load('data_files/companies_with_vectors_full.npy')

X = companies_data_array[:, 4] # company vectors
y = companies_data_array[:, 3] # industries
names = companies_data_array[:, 0] # company names

# Determine location of company vectors
# containing NaN values
def determine_nan_vector_indices(company_vectors):
    nan_vector_indices = []
    for i, vector in enumerate(company_vectors):
        for value in vector:
            if np.isnan(value):
                nan_vector_indices.append(i)
    return sorted(list(set(nan_vector_indices)))

# Reshape the array into an easier shape to handle
# namely (amount_of_company_vectors, 32), where
# 32 represents the amount of top-level industries
X_new = []
for company_vector in X:
    company_vector = company_vector.reshape(1, 32)
    X_new.append(company_vector)
X2 = np.array(X_new).reshape(X.shape[0], 32)

# Remove datapoints of comapny vectors containing NaN values
nan_vectors_indices = determine_nan_vector_indices(X2)
X_cleaned = np.delete(X2, nan_vectors_indices, 0)
y_cleaned = np.delete(y, nan_vectors_indices, 0)
names_cleaned = np.delete(names, nan_vectors_indices, 0)

# Merge the company names and the company vectors
# into one array
X_merged = np.hstack((X_cleaned, names_cleaned.reshape(names_cleaned.shape[0], 1)))

# Binarize the industry labels
y_new = []
for label in y_cleaned:
    y_new.append(label.split(';'))
mlb = MultiLabelBinarizer()
y2 = mlb.fit_transform(y_new)


industry_labels = mlb.classes_
neigh = KNeighborsClassifier(n_neighbors=3)

X_train, X_test, y_train, y_test = train_test_split(X_merged, y2,\
test_size=0.33, random_state=42)

# fit the k-nearest neighbour classifier with
# the company vectors and predict the labels of the
# test set
neigh.fit(X_train[:, 0:32], y_train)
y_pred = neigh.predict(X_test[:, 0:32])

n_test_labels = y_test.shape[0]

# for each industry, identify its false positives and
# false negatives
for i, label in enumerate(industry_labels):
    false_positives = []
    false_negatives = []

    # compare the predicted label with the true label of the test set
    # and collect the names of companies that are false positives or false negatives
    for (true_label, pred_label, index) in\
    zip(y_test[:, i], y_pred[:, i], range(n_test_labels)):
        if pred_label and not true_label:
            false_positives.append(X_test[:, -1][index])
        elif not pred_label and true_label:
            false_negatives.append(X_test[:, -1][index])

    # add a column labeling the false positives with 'FP'
    FP_dataframe = pd.DataFrame(false_positives, columns=['names'])
    FP_dataframe['type'] = pd.Series(['FP'] * FP_dataframe.shape[0])

    # add a column labeling the false positives with 'FN'
    FN_dataframe = pd.DataFrame(false_negatives, columns=['names'])
    FN_dataframe['type'] = pd.Series(['FN'] * FN_dataframe.shape[0])

    # merge false positives and false negatives dataframes together
    FN_FP_merged_df = pd.concat([FP_dataframe, FN_dataframe], axis=0, ignore_index=True)
    FN_FP_merged_df.to_csv('FN_FP_CSV_files_per_industry/FN_FP_'+ label + '.csv',\
    index=False)
