from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.metrics import f1_score, precision_score, recall_score, accuracy_score
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

'''
Classifies the TF-IDF company vectors into the top-level industries. It also
validates the classifier by calculating the accuracy, precision, recall
and the f1-score and plots for every industry a confusion matrix that quantifies
the false positives, false negatives, true positives and true negatives.
'''

# Determine location of company vectors
# containing NaN values
def determine_nan_vector_indices(company_vectors):
    nan_vector_indices = []
    for i, vector in enumerate(company_vectors):
        for value in vector:
            if np.isnan(value):
                nan_vector_indices.append(i)
    return sorted(list(set(nan_vector_indices)))

# extract the datapoints that have only
# one label
def extract_single_labeled_data(data, labels):
    indices = []
    for i, label in enumerate(labels):
        label_list = label.split(';')
        if len(label_list) == 1:
            indices.append(i)

    # use fancy indexing to filter multilabeled data
    new_labels = labels[indices]
    new_data = data[indices]
    return (new_data, new_labels)

# classify the single-labeled TF-IDF company vectors using
# k-nearest neighbours
def classify_single_label(data, labels):
    industry_labels = np.unique(labels)
    fig = plt.figure(figsize= (30, 50))

    # define classifier, train the classifier on the whole dataset
    # and predict the labels of the test set
    neigh = KNeighborsClassifier(n_neighbors=3)
    X_train, X_test, y_train, y_test = train_test_split(data, labels,\
    test_size=0.33, random_state=42)

    neigh.fit(X_train, y_train)
    y_pred = neigh.predict(X_test)

    # calculate and print the common classification metrics
    print(f1_score(y_test, y_pred, average='weighted'))
    print(precision_score(y_test, y_pred, average='weighted'))
    print(recall_score(y_test, y_pred, average='weighted'))
    print(accuracy_score(y_test, y_pred))

    # for each industry, fit the classifier and plot the confusion matrix
    for i, label in enumerate(industry_labels):

        # binarize the labels of one industry
        labels_bin = labels == label

        X_train, X_test, y_train, y_test = train_test_split(data, labels_bin,\
        test_size=0.33, random_state=42)

        neigh.fit(X_train, y_train)
        y_pred = neigh.predict(X_test)

        # calculate and plot the confusion matrix
        cm = confusion_matrix(y_test, y_pred)
        subplot = fig.add_subplot(8 ,4,i+1)
        sns.heatmap(cm, robust= True, annot= True, fmt= 'g', cmap='Blues', cbar=False, ax=subplot)
        subplot.set_title(label, size=10)
        subplot.axis('off')

    # show the confusion matrices, save the figure manually into the
    # folder 'tf_idf_plots'
    plt.show()
    return None

# classify the multi-labeled TF-IDF company vectors using
# k-nearest neighbours
def classify_multilabel(data, labels):

    # binarize the multi-labeled data
    labels_new = []
    for label in labels:
        labels_new.append(label.split(';'))
    mlb = MultiLabelBinarizer()
    mlb_labels = mlb.fit_transform(labels_new)

    industry_labels = mlb.classes_
    fig = plt.figure(figsize= (30, 50))

    # define classifier, train the classifier on the whole dataset
    # and predict the labels of the test set
    neigh = KNeighborsClassifier(n_neighbors=3)
    X_train, X_test, y_train, y_test = train_test_split(data, mlb_labels,\
    test_size=0.33, random_state=42)
    neigh.fit(X_train, y_train)
    y_pred = neigh.predict(X_test)

    # calculate and print the common classification metrics
    print(f1_score(y_test, y_pred, average='weighted'))
    print(precision_score(y_test, y_pred, average='weighted'))
    print(recall_score(y_test, y_pred, average='weighted'))
    print(accuracy_score(y_test, y_pred))

    # for each industry, calculate and plot the confusion matrix
    for i, label in enumerate(industry_labels):
        cm = confusion_matrix(y_test[:, i], y_pred[:, i])
        subplot = fig.add_subplot(8 ,4,i+1)
        sns.heatmap(cm, robust= True, annot= True, fmt= 'g', cmap='Blues', cbar=False, ax=subplot)
        subplot.set_title(label, size=10)
        subplot.axis('off')

    # show the confusion matrices, save the figure manually into the
    # folder 'tf_idf_plots'
    plt.show()
    return None

companies_data_array = np.load('data_files/companies_with_vectors_full.npy')

X = companies_data_array[:, 4] # company_vectors
y = companies_data_array[:, 3] # industries

# Reshape the array into an easier shape to handle
# namely (amount_of_company_vectors, 32), where
# 32 represents the amount of top-level industries
X_new = []
for company_vector in X:
    company_vector = company_vector.reshape(1, 32)
    X_new.append(company_vector)
X2 = np.array(X_new).reshape(X.shape[0], 32)

# Remove datapoints of comapny vectors containing NaN values
nan_vectors_indices = determine_nan_vector_indices(X2)
X_cleaned = np.delete(X2, nan_vectors_indices, 0)
y_cleaned = np.delete(y, nan_vectors_indices, 0)

# set this boolean variable to True if you want to
# work with single-labeled data
single_label = False

# run classifier
if single_label:
    X3, y = extract_single_labeled_data(X_cleaned, y_cleaned)
    classify_single_label(X3, y)
else:
    classify_multilabel(X_cleaned, y_cleaned)
