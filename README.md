[![Ontotext Logo](https://i.imgur.com/Krehiw7.png)](https://www.ontotext.com/)


# Companies Industry Classification - Ontotext Datathon

Ontotext is looking for an efficient means of classifying ~300.000 companies into 32-top-level industries using rich and complex graph-based features. At the core this is a classification problem, but it can also be framed as an error/anomaly detection task, because there is no gold standard available. To prioritize qualitative results over quantitative results we implemented 2 different models: TF-IDF, which is our naïve approach, and ELMo which is our deep contextualized approach to the solution. These models were used to create company vectors, which we then classified using K-nearest-neighbors and plotted using t-SNE. Thereafter we retrieved false positives and false negative for each of the 32-top level classes.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites
Make sure you have installed all of the following prerequisites on your local machine:

* Python 3.6+ [Download & Install](https://www.python.org/downloads/)
* Numpy [Download & Install](https://docs.scipy.org/doc/numpy/user/install.html)
* Pandas [Download & Install](https://pandas.pydata.org/pandas-docs/stable/install.html)
* AllenNLP [Download & Install](https://pypi.org/project/allennlp/)
* SKLearn [Download & Install](https://scikit-learn.org/stable/install.html)
* Matplotlib [Download & Install](https://matplotlib.org/)
* Pywikibot [Download & Install](https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation/en)
* MWParserFromHell [Download & Install](https://mwparserfromhell.readthedocs.io/en/latest/)
* Seaborn [Download & Install](https://pypi.org/project/seaborn/)
* NLTK (stopwords) [Download & Install](https://www.nltk.org/data.html)

### Installation

1. Clone this repo to your local machine using.
```
git clone https://gitlab.com/chrisalgerges98/ontotext-datathon-addax.git
```
2. Make sure all the requirements are properly installed.
```
pip install -r requirements.txt
```


## Running the models

### **TF-IDF**
#### Data
* To create an industry defining corpus, we scraped the text from Wikipedia for every top-level industry with all its corresponding sub-industries.
1. Run *generate_industries_list.py* to create a list for every top-level industry containing all the sub-industries
2. Run *generate_corpus.py* to loop over the previously generated list and scrape the Wikipedia text for every item, and save the text as a *.npy* file for every top-level industry.
3. Run *NLP_processing.py* to preprocess the scraped text, and create 1 single corpus containing 32 documents.

* The company wiki-category keywords are contained within the *tf_idf/data_files/companies.csv/* file, under the *categories* column. This file was generated using *preprocessing_ontotext_simple_data.py* file in the main repository, by preprocessing our raw data file *dt18-ontotext-simple.csv*, which is also contained there.

#### TF-IDF matrix
* The TF-IDF matrix contains all the weights for each word in the industry corpus. To create this matrix run the *tf_idf.matrix.py* file. This will output a *.csv* file containing all the weights in the matrix, and a *.npy* containing a list of all the available words in the corpus.

#### Company vectors
* To generate the TF-IDF company vectors, we need to cross reference the company keywords with the vocabulary of our generated Wikipedia corpus and then take the mean of the company keywords to create a company vector. To do this run the *company_preprocessing_tf_idf_and_TSNE.py* file placed in the main directory of our repository. The reason for the placing of this file outside of the tf_idf folder is to have a unified ordering for the companies in both our TF-IDF and ELMo company vectors, in order to later plot and compare the same exact companies using t-SNE in both models.

#### Classifying & Plotting
* To classify the company vectors run the *classification_tf_idf.py* file, this will give you all the available metrics for single and multi-label companies.  
* The *tsne_tf_idf.py* will give you a 2-dimensional plot for 10.000 single label companies.
* To retrieve all the false positives & false negatives company names, run *identify_tf_idf.py*.
These are saved in *FN_FP_CSV_files_per_industry/FN_FP_industryName.csv*.

### **ELMo**
#### Data
* For our ELMo implementation there was no necessity to create an industry-defining corpus, because ELMo comes pre-trained on a large English corpus.
* The company wiki-category keywords are contained within the *elmo/data_files/companies.csv/* file, under the *categories* column. This file was generated using the *preprocessing_ontotext_simple_data.py* file in the main repository, by preprocessing our raw data file *dt18-ontotext-simple.csv*, which is also contained there.
* Now run the *company_preprocessing_elmo.py* file to preprocess the *companies.csv* file and retrieve the company keywords saved in *elmo/data_files/company_preproc_categories.csv*.

#### Company vectors
* We used ElmoEmbedder from the AllenNLP library to create company vectors:
1. Run the *allen_elmo_company2vec.py* file to generate the company vectors.
* Generate company vectors that match the exact same ordering as the TF-IDF company vectors for later plotting:
2. Run *generate_elmo_tsne_vectors.py*

#### Classiyfing & Plotting
* To obtain the metrics for the ELMo company vectors run the *classification_elmo.py* file. This will give you the precision, recall, accuracy and F1 score for single-label and multi-label companies. Note: we use Principal Component Analysis (PCA)  here for dimensionality reduction, in order to reduce the required computational power.
* The *tsne_elmo.py* file will give you a 2-dimensional plot containing the position of the previously generated (single-label) 10.000 companies.
* To retrieve all the false positives & false negatives company names, run *identify_elmo.py*. These are saved in *FN_FP_CSV_files_per_industry/FN_FP_industryName_elmo.csv*.

When using ELMo to compute company vectors, it is recommended to run the code into a GPU, because calculating the ELMo word-embeddings requires quite a lot of computational power (It takes around five hours to compute 50000 company vectors with ELMo).

## Authors
[![UvA Logo](https://i.imgur.com/2MKibn8.png)](https://www.uva.nl/)

* **Chris al Gerges**
* **Jad Hallab**
* **Diederik Salimans**
*  **Anne Boomsma**

## License

This project is issued by [Ontotext](https://www.ontotext.com/) as part of the 2nd year end project Artificial Intelligence BSc course from the [University of Amsterdam](https://www.uva.nl/).

## Acknowledgments

* We would like to issue a special thank you to Andrey & Svelta from Ontotext for helping us obtain all the necessary information to complete this project.
* Also a special thank you also goes to our UvA mentor Wouter Zwerink for his guidance.
* Also a special thank you to Ashley Burgoyne, the head of the 2nd year end project who made all of this possible.
