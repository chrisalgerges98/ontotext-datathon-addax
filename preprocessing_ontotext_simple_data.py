import numpy as np
import pandas as pd

'''
Drops all companies that have any missing data and extracts the relevant data
of the companies.
INPUT: ontotext CSV dump.
OUTPUT: A CSV file containing the companies with relevant data
'''

ontotext_simple_df = pd.read_csv('dt18-ontotext-simple.csv')

# selects the relevant data of the companies
companies_df = ontotext_simple_df[['names', 'descriptions', 'categories', 'industries']]

# drop every company that has any missing data
companies_df = companies_df.dropna()

# save the CSV file into the folders 'tf_idf' and 'elmo'
companies_df.to_csv('tf_idf/data_files/companies.csv', index=False)
companies_df.to_csv('elmo/data_files/companies.csv', index=False)
