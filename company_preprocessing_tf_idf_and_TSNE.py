import numpy as np
import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import re

'''
Preprocesses the Wikipedia categories of the companies into company keywords
and calculates the company vectors using the TF-IDF matrix.
It also randomly samples datapoints from the dataset to use for t-SNE visualisations.
'''

# preprocess the Wikipedia categories and return a list
# of unique company keywords
def preprocess_categories(company_categories, TF_IDF_words):

    # regular expression for removing non-alphabetical symbols
    regex_letters = re.compile('[a-z]')
    categories_lower = company_categories.replace("_", " ").lower()
    categories_list = word_tokenize(categories_lower)

    # remove stop words, non-alphabetical symbols and cross-reference
    # with the words in the TF_IDF_matrix
    categories_list = [w for w in categories_list if not w in stop_words]
    categories_list = [w for w in categories_list if regex_letters.search(w)]
    categories_list = [w for w in categories_list if w in TF_IDF_words]
    return list(set(categories_list))


def company2vec(company_words, TF_IDF_matrix):

    # extract word embedding of the keywords by selecting
    # the columns of the TF_IDF_matrix corresponding to the keywords
    TF_IDF_values = TF_IDF_matrix[company_words]

    # calculate average vector over all keywords
    company_vec = TF_IDF_values.mean(axis=1)
    return company_vec.to_numpy()

def create_company_vecs_dataframe(companies_df, TF_IDF_matrix, n):
    company_vectors = []

    # for every company, calculate its company vector
    for i in range(n):
        company_data = companies_df.iloc[i]
        company_keywords = preprocess_categories(company_data['categories'], words)
        company_vec = company2vec(company_keywords, TF_IDF_matrix)
        company_vectors.append(company_vec)

    # return a dataframe of company vectors
    company_vectors_dataframe = pd.DataFrame({"company_vectors" : company_vectors})
    return company_vectors_dataframe


stop_words = set(stopwords.words('english'))
words = np.load('tf_idf/data_files/TF-IDF_words.npy')
TF_IDF_df = pd.read_csv('tf_idf/data_files/TF-IDF_matrix.csv')
companies_df = pd.read_csv('tf_idf/data_files/companies.csv')

# shuffle the dataset randomly
companies_df = companies_df.sample(frac=1).reset_index(drop=True)

# set this boolean variable to True is you want to
# sample a dataset for the t-SNE visualisations
TSNE = False

if TSNE:
    # number of samples you want to use for t-SNE
    n = 10000

    # add company vectors into the companies dataset
    company_vectors_dataframe = create_company_vecs_dataframe(companies_df, TF_IDF_df, n)
    companies_df = pd.concat([companies_df, company_vectors_dataframe], axis=1)

    # drop comapnies that have not been selected
    companies_df = companies_df.dropna()
    companies_df_array = companies_df.to_numpy()
    np.save('tf_idf/data_files/tsne_data.npy', companies_df_array)
    np.save('elmo/data_files/tsne_data.npy', companies_df_array)


else:
    total = companies_df.shape[0]

    # number of samples you want to use for classification
    n = total

    # add company vectors into the companies dataset
    company_vectors_dataframe = create_company_vecs_dataframe(companies_df, TF_IDF_df, n)
    companies_df = pd.concat([companies_df, company_vectors_dataframe], axis=1)

    # if you specified an amount of samples that is lower than
    # the total amount of companies, drop the companies that
    # have not been selected.
    if n < total:
        companies_df = companies_df.dropna()

    companies_df_array = companies_df.to_numpy()
    np.save('tf_idf/data_files/companies_with_vectors_full.npy', companies_df_array)
