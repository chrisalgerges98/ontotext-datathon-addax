from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.decomposition import PCA
from sklearn.metrics import f1_score, precision_score, recall_score, accuracy_score
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

'''
Classifies the ELMo company vectors into the top-level industries It also
validates the classifier by calculating the accuracy, precision, recall
and the f1-score and plots for every industry a confusion matrix that quantifies
the false positives, false negatives, true positives and true negatives.
Because ELMo took longer to classify, this classifier has only been tested
on 50000 samples form the companies dataset
'''

# extract the datapoints that have only
# one label
def extract_single_labeled_data(data, labels):
    indices = []
    for i, label in enumerate(labels):
        label_list = label.split(';')
        if len(label_list) == 1:
            indices.append(i)

    # use fancy indexing to filter multilabeled data
    new_labels = labels[indices]
    new_data = data[indices]
    return (new_data, new_labels)

# classify the single-labeled ELMo company vectors using
# k-nearest neighbours
def classify_single_label(data, labels):
    industry_labels = np.unique(labels)
    fig = plt.figure(figsize= (30, 50))

    # define classifier, train the classifier on the whole dataset
    # and predict the labels of the test set
    neigh = KNeighborsClassifier(n_neighbors=3)
    X_train, X_test, y_train, y_test = train_test_split(data, labels,\
    test_size=0.33, random_state=42)
    neigh.fit(X_train, y_train)
    y_pred = neigh.predict(X_test)

    # calculate and print the common classification metrics
    print(f1_score(y_test, y_pred, average='weighted'))
    print(precision_score(y_test, y_pred, average='weighted'))
    print(recall_score(y_test, y_pred, average='weighted'))
    print(accuracy_score(y_test, y_pred))

    # for each industry, fit the classifier and plot the confusion matrix
    for i, label in enumerate(industry_labels):

        # binarize the labels of one industry
        labels_bin = labels == label

        X_train, X_test, y_train, y_test = train_test_split(data, labels_bin,\
        test_size=0.33, random_state=42)

        neigh.fit(X_train, y_train)
        y_pred = neigh.predict(X_test)

        # calculate and plot the confusion matrix
        cm = confusion_matrix(y_test, y_pred)
        subplot = fig.add_subplot(8 ,4,i+1)
        sns.heatmap(cm, robust= True, annot= True, fmt= 'g', cmap='Blues', cbar=False, ax=subplot)
        subplot.set_title(label, size=10)
        subplot.axis('off')

    # show the confusion matrices, save the figure manually into the
    # folder 'tf_idf_plots'
    plt.show()
    return None

# classify the multi-labeled ELMo company vectors using
# k-nearest neighbours
def classify_multilabel(data, labels):

    # binarize the multi-labeled data
    labels_new = []
    for label in labels:
        labels_new.append(label.split(';'))
    mlb = MultiLabelBinarizer()
    mlb_labels = mlb.fit_transform(labels_new)

    industry_labels = mlb.classes_
    fig = plt.figure(figsize= (30, 50))

    # define classifier, train the classifier on the whole dataset
    # and predict the labels of the test set
    neigh = KNeighborsClassifier(n_neighbors=3)
    X_train, X_test, y_train, y_test = train_test_split(data, mlb_labels,\
    test_size=0.33, random_state=42)
    neigh.fit(X_train, y_train)
    y_pred = neigh.predict(X_test)

    # calculate and print the common classification metrics
    print(f1_score(y_test, y_pred, average='weighted'))
    print(precision_score(y_test, y_pred, average='weighted'))
    print(recall_score(y_test, y_pred, average='weighted'))
    print(accuracy_score(y_test, y_pred))

    # for each industry, calculate and plot the confusion matrix
    for i, label in enumerate(industry_labels):
        cm = confusion_matrix(y_test[:, i], y_pred[:, i])
        subplot = fig.add_subplot(8 ,4,i+1)
        sns.heatmap(cm, robust= True, annot= True, fmt= 'g', cmap='Blues', cbar=False, ax=subplot)
        subplot.set_title(label, size=10)
        subplot.axis('off')

    # show the confusion matrices, save the figure manually into the
    # folder 'tf_idf_plots'
    plt.show()
    return None

elmo_company_vectors = np.load('data_files/50k_elmo_vecs.npy')
companies_df = pd.read_csv('data_files/company_preproc_categories.csv')

X = elmo_company_vectors

# remove the piece of code '.iloc[0:n]' if you work with
# the entire dataset
y = companies_df['industries'].iloc[0:50000].to_numpy()

# perform PCA on the company vectors to reduce the dimensionality
# of the company vectors to 128
pca = PCA(n_components=128)
X2 = pca.fit_transform(X)

# set this boolean variable to True if you want to
# work with single-labeled data
single_label = False

# run classifier
if single_label:
    X3, y2 = extract_single_labeled_data(X2, y)
    classify_single_label(X3, y2)
else:
    classify_multilabel(X2, y)
