from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.decomposition import PCA
import numpy as np
import pandas as pd

'''
Identifies for each industry the false positives and the false negatives
by classifying the ELMo company vectors into the top-level industries.
The names of the companies, which are the false positives and the false negatives,
are stored into CSV files, one for every industry
'''

elmo_company_vectors = np.load('data_files/50k_elmo_vecs.npy')
companies_df = pd.read_csv('data_files/company_preproc_categories.csv')

X = elmo_company_vectors

# remove in lines 21 and 22 the piece of code '.iloc[0:n]' if you work with
# the entire dataset
y = companies_df['industries'].iloc[0:50000].to_numpy() # industries
names = companies_df['names'].iloc[0:50000].to_numpy() # company_names

# perform PCA on the company vectors to reduce the dimensionality
# of the company vectors to 128
pca = PCA(n_components=128)
X2 = pca.fit_transform(X)

# Merge the company names and the company vectors
# into one array
X_merged = np.hstack((X2, names.reshape(names.shape[0], 1)))

# Binarize the industry labels
y_new = []
for label in y:
    y_new.append(label.split(';'))
mlb = MultiLabelBinarizer()
y2 = mlb.fit_transform(y_new)


industry_labels = mlb.classes_
neigh = KNeighborsClassifier(n_neighbors=3)

X_train, X_test, y_train, y_test = train_test_split(X_merged, y2,\
test_size=0.33, random_state=42)

# fit the k-nearest neighbour classifier with
# the company vectors and predict the labels of the
# test set
neigh.fit(X_train[:, 0:128], y_train)
y_pred = neigh.predict(X_test[:, 0:128])


n_test_labels = y_test.shape[0]

# for each industry, identify its false positives and
# false negatives
for i, label in enumerate(industry_labels):
    false_positives = []
    false_negatives = []

    # compare the predicted label with the true label of the test set
    # and collect the names of companies that are false positives or false negatives
    for (true_label, pred_label, index) in\
    zip(y_test[:, i], y_pred[:, i], range(n_test_labels)):
        if pred_label and not true_label:
            false_positives.append(X_test[:, -1][index])
        elif not pred_label and true_label:
            false_negatives.append(X_test[:, -1][index])

    # add a column labeling the false positives with 'FP'
    FP_dataframe = pd.DataFrame(false_positives, columns=['names'])
    FP_dataframe['type'] = pd.Series(['FP'] * FP_dataframe.shape[0])

    # add a column labeling the false positives with 'FN'
    FN_dataframe = pd.DataFrame(false_negatives, columns=['names'])
    FN_dataframe['type'] = pd.Series(['FN'] * FN_dataframe.shape[0])

    # merge false positives and false negatives dataframes together
    FN_FP_merged_df = pd.concat([FP_dataframe, FN_dataframe], axis=0, ignore_index=True)
    FN_FP_merged_df.to_csv('FN_FP_CSV_files_per_industry/FN_FP_'+ label + '_elmo.csv',\
    index=False)
