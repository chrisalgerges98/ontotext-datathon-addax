from allennlp.commands.elmo import ElmoEmbedder
import ast
import numpy as np
import pandas as pd

'''
Calculates the company vectors using ELMo, a LSTM Recurrent Neural Network (RNN)
that is pre-trained on a very large English corpus.
'''

def elmo_company2vec(companies_df):

    # load ELMo embedder
    elmo = ElmoEmbedder()
    all_company_vectors = []
    for i in range(50000): #specify amount of companies
        company_keywords = companies_df['categories_list'].iloc[i]

        # Converts the company keywords list, which is saved as
        # a string, into a list
        company_tokens = ast.literal_eval(company_keywords)

        # calculate the word embeddings of the company keywords with ELMo
        vectors = elmo.embed_sentence(company_tokens)

        # calculate average vector over all keywords and over all
        # layers of ELMo
        vectors_mean = np.mean(vectors, axis=1)
        company_vector = np.mean(vectors_mean, axis=0)
        all_company_vectors.append(company_vector)
    np.save('data_files/50k_elmo_vecs.npy', all_company_vectors)
    return (print("finished"))

companies_df = pd.read_csv('data_files/company_preproc_categories.csv')
elmo_company2vec(companies_df)
