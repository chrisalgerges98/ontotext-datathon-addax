from allennlp.commands.elmo import ElmoEmbedder
import numpy as np
import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import re

'''
Calculates the company vectors of the t-SNE datasset using ELMo
'''

stop_words = set(stopwords.words('english'))
elmo = ElmoEmbedder()

# preprocess the Wikipedia categories and return a list
# of unique company keywords
def preprocess_categories(company_categories):

    # regular expression for removing non-alphabetical symbols
    regex_letters = re.compile('[a-z]')
    categories_lower = company_categories.replace("_", " ").lower()
    categories_list = word_tokenize(categories_lower)

    # remove stop words and non-alphabetical symbols
    categories_list = [w for w in categories_list if not w in stop_words]
    categories_list = [w for w in categories_list if regex_letters.search(w)]
    return list(set(categories_list))

def elmo_company2vec(company_tokens):
    all_company_vectors = []

    # calculate the word embeddings of the company keywords with ELMo
    vectors = elmo.embed_sentence(company_tokens)

    # calculate average vector over all keywords and over all
    # layers of ELMo
    vectors_mean = np.mean(vectors, axis=1)
    company_vector = np.mean(vectors_mean, axis=0)
    return company_vector

companies_data_array = np.load('data_files/tsne_data.npy')

all_keywords = companies_data_array[:, 2]

# calculate company vectors with ELMo for each company
all_vectors = []
companies_number = len(all_keywords)
for i in range(companies_number):
    company_keywords = preprocess_categories(all_keywords[i])
    company_vector = elmo_company2vec(company_keywords)
    all_vectors.append(company_vector)
np.save('data_files/10k_tsne_elmo.npy', all_vectors)
