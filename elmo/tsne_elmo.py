import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA

'''
Creates the t-SNE plot of the company vectors calculated using ELMo
'''

X = np.load('data_files/10k_tsne_elmo.npy')
companies_data_array = np.load('data_files/tsne_data.npy')
y = companies_data_array[:, 3]

# perform PCA on the company vectors to reduce the dimensionality
# of the company vectors to 128
pca = PCA(n_components=128)
X2 = pca.fit_transform(X)

# calculate t-SNE embeddings in 2 dimensions
X_embedded = TSNE(n_components=2).fit_transform(X2)

color_list = ['maroon', 'brown', 'olive', 'teal', 'navy', 'black', 'red', 'orange',
'yellow', 'lime', 'green', 'cyan', 'blue', 'purple', 'magenta', 'grey']
marker_list = ['+', 's']

labels = np.unique(y)
color_index = 0
marker_index = 0

# plot the t-SNE embeddings of the company vectors,
# one industry cluster at a time
for i in range(len(labels)):
    indices = np.where(y == labels[i])[0]
    plot_data = X_embedded[indices]
    plt.plot(plot_data[:, 0], plot_data[:, 1],
    marker_list[marker_index],
    color=color_list[color_index],
    label=labels[i])

    if color_index == (len(color_list) - 1):
        color_index = 0
        marker_index += 1
    else:
        color_index += 1

# Shows the t-SNE plot.
# Save the t-SNE plot manually into the folder tf_idf_plots
plt.legend()
plt.title('t-SNE plot ELMo company vectors')
plt.xlabel('t-SNE component 1')
plt.ylabel('t-SNE component 2')
plt.show()
