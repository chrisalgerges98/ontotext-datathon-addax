import numpy as np
import pandas as pd
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import re

'''
Preprocesses the Wikipedia categories of the companies into company keywords.
'''

# preprocess the Wikipedia categories and return a list
# of unique company keywords
def preprocess_categories(company_categories):

    # regular expression for removing non-alphabetical symbols
    regex_letters = re.compile('[a-z]')
    categories_lower = company_categories.replace("_", " ").lower()
    categories_list = word_tokenize(categories_lower)

    # remove stop words, non-alphabetical symbols
    categories_list = [w for w in categories_list if not w in stop_words]
    categories_list = [w for w in categories_list if regex_letters.search(w)]
    return list(set(categories_list))

def create_preproc_categ_dataframe(companies_df, n):
    categs = []

    # for every company, preprocess its Wikipedia categories
    # into company keywords
    for i in range(n):
        company_data = companies_df.iloc[i]
        company_keywords = preprocess_categories(company_data['categories'])
        categs.append(company_keywords)

    # return a dataframe of company keywords
    company_categs_dataframe = pd.DataFrame({"categories_list" : categs})
    return company_categs_dataframe

stop_words = set(stopwords.words('english'))
companies_df = pd.read_csv('data_files/companies.csv')

# shuffle the dataset randomly
companies_df = companies_df.sample(frac=1).reset_index(drop=True)

total = companies_df.shape[0]
n = total

# add company keywords into the companies dataset
company_categs_dataframe = create_preproc_categ_dataframe(companies_df, n)
companies_df = pd.concat([companies_df, company_categs_dataframe], axis=1)

# if you specified an amount of samples that is lower than
# the total amount of companies, drop the companies that
# have not been selected.
if n < companies_df.shape[0]:
    companies_df = companies_df.dropna()

companies_df.to_csv('data_files/company_preproc_categories.csv', index=False)
